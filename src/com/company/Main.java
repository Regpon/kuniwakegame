package com.company;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner sc1 = new Scanner(System.in);

        List<String> lines = new ArrayList<>();
        while (sc1.hasNext()) {
            lines.add(sc1.next());
        }

        // 最初の2文字でmap作る
        Map<String, String> countryMap = new HashMap<String, String>(){
            {
                put("アメ", "アメリカ");
                put("イギ", "イギリス");
                put("イン", "インドネシア");
                put("ブラ", "ブラジル");
                put("アル", "アルゼンチン");
                put("オー", "オーストラリア");
                put("エジ", "エジプト");
            }
        };

        lines.forEach(
            line -> System.out.println(
                countryMap.get(line.substring(0, 2))
                    .substring(line.length())
            )
        );


    }
}
